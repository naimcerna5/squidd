<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingreso de Datos</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

<nav class="navbar bg-dark border-bottom border-body" data-bs-theme="dark">
  <!-- Navbar content -->
  <nav class="navbar bg-body-tertiary">
  <div class="container-fluid">
    <a class="navbar-brand">Navbar</a>
    <form class="d-flex" role="search">
      <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success" type="submit">Search</button>
    </form>
  </div>
</nav>
</nav>
<br>
<br>
<br>
<br>
<body>
    <div class="container text-center">
        <div class="row">
            <div class="col-md-6">
              <form action="procesar.php" method="post">
                <div class="form-group">
                  <label for="numero_puerto" class="font-weight-bold text-left"></label>
                  <input type="number" id="numero_puerto" name="numero_puerto" class="form-control" placeholder="Número de Puerto:" required>
                </div>
                <div class="form-group">
                  <label for="departamento" class="font-weight-bold text-left">Selecciona un Departamento:</label>
                  <select id="departamento" name="departamento" class="form-control" required>
                      <option value="departamento1">Ventas</option>
                      <option value="departamento2">Marketing</option>
                      <option value="departamento3">Recursos Humanos</option>
                  </select>
                </div>

                <br>
                <div class="form-group">
                  <input type="text" id="nombre_hostname" name="nombre_hostname" class="form-control" placeholder="Nombre del Hostname" required>
                </div>
                <div class="form-group">
                  <label for="total_memoria" class="font-weight-bold text-left"></label>
                  <input type="number" id="total_memoria" name="total_memoria" class="form-control"  placeholder="Total de Memoria:" required>
                </div>
                <div class="form-group">
                  <label for="dns" class="font-weight-bold text-left"></label>
                  <input type="text" id="dns" name="dns" class="form-control"   placeholder="DNS:" required>
                </div>
                <div class="form-group">
                  <label for="nombre_acl" class="font-weight-bold text-left"></label>
                  <input type="text" id="nombre_acl" name="nombre_acl" class="form-control"   placeholder="Nombre de ACL:" required>
                </div>
                <div class="form-group">
                  <label for="allow" class="font-weight-bold text-left"></label>
                  <input type="text" id="allow" name="allow" class="form-control"   placeholder="Allow:" required>
                </div>
                <div class="form-group">
                  <label for="acls" class="font-weight-bold text-left"></label>
                  <textarea id="acls" name="acls" class="form-control" rows="4"   placeholder="Lista de ACLs:" required></textarea>
                </div>
                <input type="submit" value="Guardar" class="btn btn-primary">
              </form>
            </div>
            <div class="col-md-6">
                <div class="image-container">
                    <img src="https://media.licdn.com/dms/image/D5612AQG5j9IX4e1BIg/article-cover_image-shrink_600_2000/0/1662063920935?e=2147483647&v=beta&t=E_wJPwQstIuo3guu1bii3BP3HQpFYpSGMn_pWKdrnl0" alt="">
                </div>
                <div class="service-control mt-4">
                    <h2>Control de Servicio Squid</h2>
                    <form method="post">
                        <input type="submit" name="restart_squid" value="Reiniciar Squid" class="btn btn-success">
                        <input type="submit" name="start_squid" value="Iniciar Squid" class="btn btn-success">
                        <input type="submit" name="stop_squid" value="Detener Squid" class="btn btn-danger">
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>





     <?php
     if ($_SERVER["REQUEST_METHOD"] == "POST") {
         if (isset($_POST['restart_squid'])) {
             $output = shell_exec('sudo systemctl restart squid 2>&1');
             if ($output) {
                 echo "<p style='color:green;'>$output</p>";
             } else {
                 echo "<p style='color:red;'>Ha ocurrido un error al reiniciar Squid.</p>";
             }
         } elseif (isset($_POST['start_squid'])) {
             $output = shell_exec('sudo systemctl start squid 2>&1');
             if ($output) {
                 echo "<p style='color:green;'>$output</p>";
             } else {
                 echo "<p style='color:red;'>Ha ocurrido un error al iniciar Squid.</p>";
             }
         } elseif (isset($_POST['stop_squid'])) {
             $output = shell_exec('sudo systemctl stop squid 2>&1');
             if ($output) {
                 echo "<p style='color:green;'>$output</p>";
             } else {
                 echo "<p style='color:red;'>Ha ocurrido un error al detener Squid.</p>";
             }
         }
     }
     ?>



</html>
