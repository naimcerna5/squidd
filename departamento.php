<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingreso de ACLs por Departamento</title>
</head>
<body>
    <h2>Ingreso de ACLs por Departamento</h2>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <label for="departamento">Selecciona un Departamento:</label>
        <select id="departamento" name="departamento" required>
            <option value="departamento1">Departamento 1</option>
            <option value="departamento2">Departamento 2</option>
            <option value="departamento3">Departamento 3</option>
        </select><br><br>

        <label for="acls">Lista de ACLs:</label><br>
        <textarea id="acls" name="acls" rows="4" cols="50" required></textarea><br><br>

        <input type="submit" value="Guardar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Recopilar los datos del formulario
        $numero_puerto = $_POST['numero_puerto'];
        $nombre_hostname = $_POST['nombre_hostname'];
        $total_memoria = $_POST['total_memoria'];
        $dns = $_POST['dns'];
        $nombre_acl = $_POST['nombre_acl'];
        $allow = $_POST['allow'];
        $departamento = $_POST['departamento'];
        $acls = $_POST['acls'];

        // Ruta al archivo de texto correspondiente al departamento seleccionado
        $ruta_archivos = 'C:\xampp\htdocs\squid';
        $archivo_departamento = '';

        switch ($departamento) {
            case 'departamento1':
                $archivo_puerto = 'C:\xampp\htdocs\squid\puerto .txt';
                $archivo_hostname = 'C:\xampp\htdocs\squid\hostname.txt';
                $archivo_memoriacache = 'C:\xampp\htdocs\squid\memoriacache.txt';
                $archivo_dns = 'C:\xampp\htdocs\squid\dns.txt';
                $archivo_aclinternet = 'C:\xampp\htdocs\squid\aclinternet.txt';
                $archivo_allow = 'C:\xampp\htdocs\squid\allow.txt';
                $archivo_departamento = 'C:\xampp\htdocs\squid\departamento1.txt';

                break;
            case 'departamento2':
                $archivo_departamento = 'C:\xampp\htdocs\squid\departamento2.txt';
                $archivo_puerto = 'C:\xampp\htdocs\squid\puerto2.txt';
                $archivo_hostname = 'C:\xampp\htdocs\squid\hostname2.txt';
                $archivo_memoriacache = 'C:\xampp\htdocs\squid\memoriacache2.txt';
                $archivo_dns = 'C:\xampp\htdocs\squid\dns2.txt';
                $archivo_aclinternet = 'C:\xampp\htdocs\squid\aclinternet2.txt';
                $archivo_allow = 'C:\xampp\htdocs\squid\allow2.txt';
                break;
            case 'departamento3':
                $archivo_departamento = 'C:\xampp\htdocs\squid\departamento3.txt';
                $archivo_puerto = 'C:\xampp\htdocs\squid\puerto3.txt';
                $archivo_hostname = 'C:\xampp\htdocs\squid\hostname3.txt';
                $archivo_memoriacache = 'C:\xampp\htdocs\squid\memoriacache3.txt';
                $archivo_dns = 'C:\xampp\htdocs\squid\dns3.txt';
                $archivo_aclinternet = 'C:\xampp\htdocs\squid\aclinternet3.txt';
                $archivo_allow = 'C:\xampp\htdocs\squid\allow3.txt';


                break;
            default:
                echo "Error: Departamento no válido.";
                exit;
        }

        // Escribir las nuevas ACLs en el archivo de texto del departamento correspondiente
        file_put_contents($archivo_departamento, $acls);
        file_put_contents($archivo_puerto, $numero_puerto . "\n");
        file_put_contents($archivo_hostname, $nombre_hostname . "\n");
        file_put_contents($archivo_memoriacache, $total_memoria . "\n");
        file_put_contents($archivo_dns, $dns . "\n");
        file_put_contents($archivo_aclinternet, $nombre_acl . "\n");
        file_put_contents($archivo_allow, $allow . "\n");

        // Almacenar los datos en la base de datos MySQL
        $servidor = "localhost";
        $usuario = "root";
        $contraseña = "";
        $base_de_datos = "squid";

        // Crear conexión
        $conn = new mysqli($servidor, $usuario, $contraseña, $base_de_datos);

        // Verificar la conexión
        if ($conn->connect_error) {
            die("Error de conexión: " . $conn->connect_error);
        }

        // Preparar y ejecutar la consulta para insertar los datos en la base de datos
        $sql = "INSERT INTO acls_departamento (departamento, acls) VALUES ('$departamento', '$acls')";
        $sql_puerto = "INSERT INTO puerto (numero_puerto) VALUES ('$numero_puerto')";
        $sql_hostname = "INSERT INTO hostname (nombre_hostname) VALUES ('$nombre_hostname')";
        $sql_memoriacache = "INSERT INTO memoriacache (total_memoria) VALUES ('$total_memoria')";
        $sql_dns = "INSERT INTO dns (dns) VALUES ('$dns')";
        $sql_aclinternet = "INSERT INTO aclinternet (nombre_acl) VALUES ('$nombre_acl')";
        $sql_allow = "INSERT INTO allow (allow) VALUES ('$allow')";


        if ($conn->query($sql) === TRUE) {
            echo "<p>Las ACLs se han guardado correctamente en el archivo del departamento seleccionado y en la base de datos.</p>";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        // Cerrar conexión
        $conn->close();
    }
    ?>


</body>
</html>
