-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-05-2024 a las 18:54:29
-- Versión del servidor: 10.4.32-MariaDB
-- Versión de PHP: 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `squid`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aclinternet`
--

CREATE TABLE `aclinternet` (
  `id` int(11) NOT NULL,
  `nombre_acl` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `aclinternet`
--

INSERT INTO `aclinternet` (`id`, `nombre_acl`) VALUES
(1, 'www.google.com'),
(2, 'www.google.com'),
(3, 'www.google.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acls_departamento`
--

CREATE TABLE `acls_departamento` (
  `id` int(11) NOT NULL,
  `departamento` varchar(50) NOT NULL,
  `acls` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `acls_departamento`
--

INSERT INTO `acls_departamento` (`id`, `departamento`, `acls`) VALUES
(1, 'departamento1', ''),
(2, 'departamento1', ''),
(3, 'departamento1', ''),
(4, 'departamento1', 'www.com'),
(5, 'departamento2', 'www.yotube.comn'),
(6, 'departamento1', 'www.googler.com'),
(7, 'departamento3', 'www.google.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `allow`
--

CREATE TABLE `allow` (
  `id` int(11) NOT NULL,
  `allow` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `allow`
--

INSERT INTO `allow` (`id`, `allow`) VALUES
(1, '170.8.8.0/8'),
(2, '170.8.8.0/8'),
(3, '170.8.8.0/8');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dns`
--

CREATE TABLE `dns` (
  `id` int(11) NOT NULL,
  `dns` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `dns`
--

INSERT INTO `dns` (`id`, `dns`) VALUES
(1, '8.8.8.8'),
(2, '8.8.8.8'),
(3, '8.8.8.8');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hostname`
--

CREATE TABLE `hostname` (
  `id` int(11) NOT NULL,
  `nombre_hostname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `hostname`
--

INSERT INTO `hostname` (`id`, `nombre_hostname`) VALUES
(1, '.com'),
(2, '.com'),
(3, 'cmsoiacma.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memoriacache`
--

CREATE TABLE `memoriacache` (
  `id` int(11) NOT NULL,
  `total_memoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `memoriacache`
--

INSERT INTO `memoriacache` (`id`, `total_memoria`) VALUES
(1, 800),
(2, 800),
(3, 800);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerto`
--

CREATE TABLE `puerto` (
  `id` int(11) NOT NULL,
  `numero_puerto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `puerto`
--

INSERT INTO `puerto` (`id`, `numero_puerto`) VALUES
(1, 8080),
(2, 8080),
(3, 8080);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aclinternet`
--
ALTER TABLE `aclinternet`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `acls_departamento`
--
ALTER TABLE `acls_departamento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `allow`
--
ALTER TABLE `allow`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dns`
--
ALTER TABLE `dns`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hostname`
--
ALTER TABLE `hostname`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `memoriacache`
--
ALTER TABLE `memoriacache`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `puerto`
--
ALTER TABLE `puerto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aclinternet`
--
ALTER TABLE `aclinternet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `acls_departamento`
--
ALTER TABLE `acls_departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `allow`
--
ALTER TABLE `allow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `dns`
--
ALTER TABLE `dns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `hostname`
--
ALTER TABLE `hostname`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `memoriacache`
--
ALTER TABLE `memoriacache`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `puerto`
--
ALTER TABLE `puerto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
